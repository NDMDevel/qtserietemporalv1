/********************************************************************************
** Form generated from reading UI file 'MainWin.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWIN_H
#define UI_MAINWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>
#include "qcustomplot/qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWin
{
public:
    QVBoxLayout *verticalLayout;
    QPushButton *init;
    QPushButton *stop;
    QHBoxLayout *horizontalLayout_2;
    QCheckBox *recordVideo;
    QSlider *horizontalSlider;
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QLabel *VideoOutput;
    QCustomPlot *customPlot;

    void setupUi(QDialog *MainWin)
    {
        if (MainWin->objectName().isEmpty())
            MainWin->setObjectName(QStringLiteral("MainWin"));
        MainWin->resize(440, 409);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWin->sizePolicy().hasHeightForWidth());
        MainWin->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(MainWin);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        init = new QPushButton(MainWin);
        init->setObjectName(QStringLiteral("init"));

        verticalLayout->addWidget(init);

        stop = new QPushButton(MainWin);
        stop->setObjectName(QStringLiteral("stop"));

        verticalLayout->addWidget(stop);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        recordVideo = new QCheckBox(MainWin);
        recordVideo->setObjectName(QStringLiteral("recordVideo"));

        horizontalLayout_2->addWidget(recordVideo);

        horizontalSlider = new QSlider(MainWin);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        frame = new QFrame(MainWin);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setLayoutDirection(Qt::LeftToRight);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        VideoOutput = new QLabel(frame);
        VideoOutput->setObjectName(QStringLiteral("VideoOutput"));
        VideoOutput->setGeometry(QRect(10, 50, 48, 16));
        sizePolicy.setHeightForWidth(VideoOutput->sizePolicy().hasHeightForWidth());
        VideoOutput->setSizePolicy(sizePolicy);
        VideoOutput->setLayoutDirection(Qt::LeftToRight);
        VideoOutput->setAutoFillBackground(false);
        VideoOutput->setFrameShape(QFrame::Box);
        VideoOutput->setFrameShadow(QFrame::Plain);
        VideoOutput->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(frame);

        customPlot = new QCustomPlot(MainWin);
        customPlot->setObjectName(QStringLiteral("customPlot"));

        horizontalLayout->addWidget(customPlot);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(MainWin);

        QMetaObject::connectSlotsByName(MainWin);
    } // setupUi

    void retranslateUi(QDialog *MainWin)
    {
        MainWin->setWindowTitle(QApplication::translate("MainWin", "MainWin", 0));
        init->setText(QApplication::translate("MainWin", "Init Capture", 0));
        stop->setText(QApplication::translate("MainWin", "Stop Capture", 0));
        recordVideo->setText(QApplication::translate("MainWin", "Record Video", 0));
        VideoOutput->setText(QApplication::translate("MainWin", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWin: public Ui_MainWin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWIN_H
