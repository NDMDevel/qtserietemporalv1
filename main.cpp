/*
 * File:   main.cpp
 * Author: Escritorio
 *
 * Created on 9 de junio de 2015, 17:41
 */

#include <QApplication>
#include "MainWin.h"

int main(int argc, char *argv[])
{
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QApplication app(argc, argv);
    
    MainWin win;
    win.show();
    // create and show your widgets here

    return app.exec();
}
