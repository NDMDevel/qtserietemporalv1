/*
 * File:   MainWin.cpp
 * Author: Escritorio
 *
 * Created on 9 de junio de 2015, 17:49
 */
 
#include "MainWin.h"
#include <QtCore>
#include <QThread>
// **** PUBLIC:
MainWin::MainWin()
{
    widget.setupUi(this);
    //inicio comunicacion con la camara
    video.open(Logitech);
    //capturo propiedades basicas de la camara para asegurar la correcta conexcion
    int fps = video.get(CV_CAP_PROP_FPS);
    int height = video.get(CV_CAP_PROP_FRAME_HEIGHT);
    int width= video.get(CV_CAP_PROP_FRAME_WIDTH);
    //muestro en consola las propiedades
    qDebug() << "fps: " << fps << " height: " << height << " width: " << width;

    // ***IMPORTANTE:
    //Esta linea es importante para poder pasar QVectors<double> como argmentos
    //en signals a slots
    qRegisterMetaType<QVector<double> >("QVector<double>");
    qRegisterMetaType<QCustomPlot::RefreshPriority>("QCustomPlot::RefreshPriority");

    this->setWindowFlags(Qt::Window);//habilita los botones de minimizar y
                                     //restaurar en la ventana principal
    serieLength = 100;
    terminateThread = true;
    updateFrame = true;
    initRect = true;
    drawLines = false;
    drawRect = false;
    connect(widget.init,SIGNAL(clicked()),this,SLOT(on_initCapture_click(void)));
    connect(widget.stop,SIGNAL(clicked()),this,SLOT(on_stopCapture_click(void)));
    connect(this,SIGNAL(updatePixmap(const QPixmap &)),widget.VideoOutput,SLOT(setPixmap(const QPixmap &)));
//    widget.VideoOutput->installEventFilter(this);
}

MainWin::~MainWin()
{
    terminateThread = true;
    drawRect = false;
    camera.waitForFinished();
    recorder.release();
}
// END PUBLIC ****


// **** PRIVATE:
void MainWin::updateVideo(void)
{
    //Mat en el que se guardaran los frames capturados desde la camara
    Mat frame;
//    widget.VideoOutput->resize(frame.cols,frame.rows);
//    while(1)
//    {
//        widget.VideoOutput->setPixmap(QPixmap::fromImage(*img));
//        video >> frame;
//    }
    Mat fcopy;
    video >> fcopy;
    QImage qimg(fcopy.data,fcopy.cols,fcopy.rows,QImage::Format_RGB888);
    int N=0;
    for( int i=0 ; i<serieLength ; i++ )
        plotX.append(i);

    widget.VideoOutput->resize(video.get(CV_CAP_PROP_FRAME_WIDTH),video.get(CV_CAP_PROP_FRAME_HEIGHT));
    while(!terminateThread)
    {
        //tomo una SnapShot
        if( updateFrame )
            video.read(frame);
        frame.copyTo(fcopy);

        //muestro la snapshot en el formulario
        if( drawRect )
            rectangle(fcopy,p1,p2,Scalar(0xFF,0x70,0x00),2);
        if( drawLines )
        {
            line(fcopy,Point(p3.x,0),Point(p3.x,video.get(CV_CAP_PROP_FRAME_HEIGHT)),Scalar(0x00,0xFF,0x00));
            line(fcopy,Point(0,p3.y),Point(video.get(CV_CAP_PROP_FRAME_WIDTH),p3.y),Scalar(0x00,0xFF,0x00));
            if( N == serieLength-1 )
                plotY.removeAt(0);
            else
                N++;
            plotY.append((frame.at<Vec3b>(p3.x,p3.y)[0]+frame.at<Vec3b>(p3.x,p3.y)[1]+frame.at<Vec3b>(p3.x,p3.y)[3])/3);
            emit setCustomPlotData(plotX,plotY);
//            widget.customPlot->graph(0)->setData(plotX,plotY);
            // set axes ranges, so we see all data:
            emit updateCustomPlot(QCustomPlot::rpHint);
//            widget.customPlot->replot();
        }
        
        if( widget.recordVideo->isChecked() )
        {
            if( !recorder.isOpened() )
                recorder.open("VideoOut.avi",CV_FOURCC_DEFAULT,15,cv::Size(video.get(CV_CAP_PROP_FRAME_WIDTH),video.get(CV_CAP_PROP_FRAME_HEIGHT)));
            recorder.write(fcopy);  //guarda el frame en el video
        }
//        widget.VideoOutput->setPixmap(QPixmap::fromImage(MatToQImage(fcopy)));
        //widget.VideoOutput->setPixmap(QPixmap::fromImage(*img));
        emit updatePixmap(QPixmap::fromImage(qimg));
        QThread::msleep(1000/25);
        //waitKey(1000);
    }
    qDebug() << "Exiting Thread";
}

QImage MainWin::MatToQImage(const Mat& mat)
{
    // 8-bits unsigned, NO. OF CHANNELS=1
    if(mat.type()==CV_8UC1)
    {
        // Set the color table (used to translate colour indexes to qRgb values)
        QVector<QRgb> colorTable;
        for (int i=0; i<256; i++)
            colorTable.push_back(qRgb(i,i,i));
        // Copy input Mat
        const uchar *qImageBuffer = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
        img.setColorTable(colorTable);
        return img;
    }
    // 8-bits unsigned, NO. OF CHANNELS=3
    else if(mat.type()==CV_8UC3)
    {
        // Copy input Mat
        const uchar *qImageBuffer = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
        return img.rgbSwapped();
    }
    else
    {
        qDebug() << "ERROR: Mat could not be converted to QImage.";
        return QImage();
    }
}
void MainWin::mousePressEvent(QMouseEvent *event)
{
    if( event->button() == Qt::MiddleButton )
    {
        initLines = false;
        initRect = false;
    }
    if( event->button() == Qt::LeftButton )
    {
        //qDebug() << "Click" << "x:" << event->localPos().x() << "y:" << event->localPos().y();
//        qDebug() << widget.frame->geometry().contains(event->localPos().x(),event->localPos().y());
        //qDebug() << "xL:" << widget.VideoOutput->pos().x() << "yL:" << widget.VideoOutput->pos().y();
//        int x1,y1,x2,y2;
//        qDebug() << x1 << " " << y1 << " " << x2 << " " << y2 << " - " << widget.VideoOutput->hasMouseTracking();
        
        //widget.VideoOutput->getContentsMargins(&x1,&y1,&x2,&y2);
        //if( x1<=event->localPos().x() && x2>=event->localPos().x() &&
        //    y1<=event->localPos().y() && y2>=event->localPos().y() )
        int x = event->localPos().x();
        int y = event->localPos().y();
        if( mouseInsideVideo(&x,&y) )
            qDebug() << "in";
        else
            qDebug() << "out";
        qDebug() << "InsideX:" << x << "InsideY:" << y;
        if( terminateThread==false )
        {
//            if( initRect == true )
            {
                p1.x = x;
                p1.y = y;
                p2.x = x;
                p2.y = y;
                initRect = true;
            }
        }
        initLines = false;
    }
    if( event->button() == Qt::RightButton )
    {
//        widget.customPlot->graph(0)->setData(plotX,plotY);
        initLines = true;
        initRect = false;
    }
}
void MainWin::mouseMoveEvent(QMouseEvent *event)
{
    if( terminateThread==false )
    {
        int x = event->localPos().x();
        int y = event->localPos().y();
        if( !mouseInsideVideo(&x,&y) )
            return;
//        qDebug() << event->button();
        if( initLines == true )
        {
            p3.x = x;
            p3.y = y;
            drawLines = true;
        }
        if( initRect == true )
        {
            p2.x = x;
            p2.y = y;
            drawRect = true;
        }
    }
}
void MainWin::mouseReleaseEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton )
    {
        qDebug() << "Release";
        if( terminateThread==false )
        {
            int x = event->pos().x();
            int y = event->pos().y();
            mouseInsideVideo(&x,&y);
            p2.x = x;
            p2.y = y;
            initRect = false;
            if( p1 == p2 )
                drawRect = false;
            else
                drawRect = true;
        }
    }
    if( event->button() == Qt::RightButton )
    {
        initLines = false;
    }
}
void MainWin::wheelEvent(QWheelEvent *event)
{
    if( event->delta()>0 )
        qDebug() << "Wheel up";
    else
        qDebug() << "Wheel down";
}
/*bool MainWin::eventFilter(QObject *obj,QEvent *event)
{
    if( event->type()!= QEvent::Paint )
    {
        qDebug() << obj->objectName();// << "Event on VideoOutput";
    }
    return true;
}*/
bool MainWin::mouseInsideVideo(int *x,int *y)
{
    bool retval;
    if( widget.VideoOutput->pos().x()+widget.frame->pos().x()<=*x && widget.VideoOutput->pos().x()+widget.frame->pos().x()+640>=*x &&
        widget.VideoOutput->pos().y()+widget.frame->pos().y()<=*y && widget.VideoOutput->pos().y()+widget.frame->pos().y()+480>=*y )
        retval = true;
    else
        retval = false;
    *x -= widget.VideoOutput->pos().x()+widget.frame->pos().x()+1;
    *y -= widget.VideoOutput->pos().y()+widget.frame->pos().y()+1;
    return retval;
}
// END PRIVATE ****

// **** PRIVATE SLOTS:
void MainWin::on_initCapture_click()
{
    //esta corriendo el thread?
    if( terminateThread == true)
    {
        widget.customPlot->addGraph();
        widget.customPlot->xAxis->setLabel("frame");
        widget.customPlot->yAxis->setLabel("Pix RGB Average Value");
        widget.customPlot->xAxis->setRange(0,serieLength);
        widget.customPlot->yAxis->setRange(0,255);
        connect(this,SIGNAL(setCustomPlotData(const QVector<double> &, const QVector<double> &)),
                widget.customPlot->graph(0),SLOT(setData(const QVector<double> &, const QVector<double> &)));
        connect(this,SIGNAL(updateCustomPlot(QCustomPlot::RefreshPriority)),
                widget.customPlot,SLOT(replot(QCustomPlot::RefreshPriority)));
        camera = QtConcurrent::run(this,&MainWin::updateVideo);
    }
    terminateThread = false;
    updateFrame = true;
    
    //QCustomPlot test
/*    QVector<double> x(101), y(101); // initialize with entries 0..100
    for (int i=0; i<101; ++i)
    {
      x[i] = i/50.0 - 1; // x goes from -1 to 1
      y[i] = x[i]*x[i]; // let's plot a quadratic function
    }
    // create graph and assign data to it:
    widget.customPlot->addGraph();
//    widget.customPlot->addGraph();
    widget.customPlot->graph(0)->setData(x,y);
//    widget.customPlot->graph(1)->setData(y,x);
    // give the axes some labels:
    widget.customPlot->xAxis->setLabel("x");
    widget.customPlot->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    widget.customPlot->xAxis->setRange(-1, 1);
    widget.customPlot->yAxis->setRange(0, 1);
    widget.customPlot->replot();*/
}
void MainWin::on_stopCapture_click()
{
    //esta corriendo el thread?
    updateFrame = false;
}
// END PRIVATE SLOTS ****
