/* 
 * File:   MainWin.h
 * Author: Escritorio
 *
 * Created on 9 de junio de 2015, 17:50
 */

#ifndef _MAINWIN_H
#define	_MAINWIN_H

#include "ui_MainWin.h"
#include <QDebug>
#include <QtCore>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QtGui>
#include <QVector>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/video.hpp"
#include "qcustomplot/qcustomplot.h"

//camera ID
#define Logitech 0
using namespace cv;

class MainWin : public QDialog
{
    Q_OBJECT
private:
    Ui::MainWin widget;
    VideoCapture video;
    VideoWriter recorder;
    QFuture<void> camera;
    QVector<double> plotX;
    QVector<double> plotY;
    int serieLength;
    bool updateFrame;
    bool terminateThread;
    bool initRect;
    bool initLines;
    bool drawRect;
    bool drawLines;
    Point p1,p2,p3;
    void updateVideo(void);
    QImage MatToQImage(const Mat& mat);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
//    bool eventFilter(QObject *obj,QEvent *event);
    bool mouseInsideVideo(int *x,int *y);
signals:
    void updatePixmap(const QPixmap &pixMap);
    void setCustomPlotData(const QVector<double> &key, const QVector<double> &value);
    void updateCustomPlot(QCustomPlot::RefreshPriority refreshPriority=QCustomPlot::rpHint);
private slots:
    void on_initCapture_click(void);
    void on_stopCapture_click(void);
public:
    MainWin();
    virtual ~MainWin();
};

#endif	/* _MAINWIN_H */
