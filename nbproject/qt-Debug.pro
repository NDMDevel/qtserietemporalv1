# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Debug/MinGW-Windows
TARGET = qtserietemporalv1
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += debug 
PKGCONFIG +=
QT = core gui widgets printsupport
SOURCES += MainWin.cpp main.cpp qcustomplot/qcustomplot.cpp
HEADERS += MainWin.h qcustomplot/qcustomplot.h
FORMS += MainWin.ui
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Debug/MinGW-Windows
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += /G/opencv/mybuild2/install/include qcustomplot/qcustomplot.h 
LIBS += 
LIBS+=G:\opencv\mybuild2\install\x86\mingw\bin\libopencv_core249.dll
LIBS+=G:\opencv\mybuild2\install\x86\mingw\bin\libopencv_highgui249.dll
LIBS+=G:\opencv\mybuild2\install\x86\mingw\bin\libopencv_imgproc249.dll
LIBS+=G:\opencv\mybuild2\install\x86\mingw\bin\libopencv_video249.dll
