# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Release/MinGW-Windows
TARGET = qtserietemporalv1
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += release 
PKGCONFIG +=
QT = core gui widgets printsupport
SOURCES += MainWin.cpp main.cpp qcustomplot/qcustomplot.cpp
HEADERS += MainWin.h qcustomplot/qcustomplot.h
FORMS += MainWin.ui
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/MinGW-Windows
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += /Z\OpenCV_All_QTminGW_Vtk_Python_Java\include 
LIBS += 
LIBS+=Z:\OpenCV_All_QTminGW_Vtk_Python_Java\x64\mingw\bin\libopencv_core249.dll
LIBS+=Z:\OpenCV_All_QTminGW_Vtk_Python_Java\x64\mingw\bin\libopencv_highgui249.dll
LIBS+=Z:\OpenCV_All_QTminGW_Vtk_Python_Java\x64\mingw\bin\libopencv_imgproc249.dll
LIBS+=Z:\OpenCV_All_QTminGW_Vtk_Python_Java\x64\mingw\bin\libopencv_video249.dll
