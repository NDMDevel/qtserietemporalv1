TODO:
Resolver problema de crash cuando se cambia (con click derecho) el pixel cuya
serie tempporal se muetra on-the-fly en el QCustomPlot.
(Pista: El evento mouseMove modifica una variable tipo cv::Point que no se
envia mediante SIGNLAS & SLOTS al thread que captura frames y los muestra en
QLabel, sino que se lo modifica directamete como si se tratara del mismo
thread de ejecucion. Esto puede ser el origen del problema.
Posiblemente haya que encapsular el proceso de captura y visualizacion en un
nuevo QObject para enviar el punto mediantes SIGNALS & SLOTS).

Cambios/Updates:
1)  Se muestra cv::Mat capturado en tiempo real desde la camara en Qt::QLabel
    usando conversion pixel a pixel desde Mat a QPixmap.
2)  Se agrega capacidad de generar un video con lo mostrado en el QLabel.
3)  Se agrega QCustomPlot, que permite realizar graficos en el el plano X-Y-
4)  Se agrega posibilidad de crear rectangulos en pantalla con clic izq.
5)  Se agrega capacidad de marcar un punto/pixel en pantalla con clic derecho.
6)  Con el punto marcado en la pantalla, se toma la serie temporal de ese pixel
    y se lo muestra en el QCustomPlot.
6)  Se resuelve el bug que hacia colapsar la aplicacion por deteccion de llamado
    recursivo al metodo repaint() al redimencionamiento del QLabel. Ahora se
    envian los datos del nuevo frame capturado al QLabel a traves del mecanismo
    SIGNAL/SLOTS.
7)  Se mejora la eficiencia en el proceso de mostrar la cv::Mat en el QLabel
    vinculando los objetos directamente, en lugar de convertil los puntos pixel
    a pixel. (Ahora el proceso de conversion de un formato a otro no se realiza)
